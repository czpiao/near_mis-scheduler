#include "Utility.h"

alg_info degree_one_reduction()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "one";
    info.del_m = info.del_n = 0;

    while(!que[0].empty())
    {
        int u = que[0].front();
        que[0].pop();
        if(del[u] || g[u].degree>1) continue;
        if(g[u].degree==1)
        {
            int i = 0;
            while(del[edge[u][i]]) i++;
            int v = edge[u][i];
            info.del_n ++;
            info.del_m += g[v].degree;
            del_node(v);
        }
        del[u] = true;
        --num_d0;
        ++info.del_n;
        edge[u].clear();
        g[u].mis = true;
        ++misnum;
    }

    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

alg_info degree_two_reduction()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "two";
    info.del_m = info.del_n = 0;

    while(!que[1].empty())
    {
        int u = que[1].front();
        que[1].pop();
        if(del[u] || g[u].degree!=2) continue;
        int i = 0;
        while(del[edge[u][i]]) i++;
        int v = edge[u][i];
        i++;
        while(del[edge[u][i]]) i++;
        int w = edge[u][i];
        if(g[v].degree<g[w].degree)
        {
            w = v;
            v = edge[u][i];
        }
        if(exist_edge(v,w))
        {
            info.del_n += 3;
            info.del_m += g[v].degree;
            del_node(v);
            info.del_m += g[w].degree;
            del_node(w);
            del[u] = true;
            --num_d0;
            edge[u].clear();
            g[u].mis = true;
            ++misnum;
        }
        else{
            merge_log.push_back(merge_type(u,v,w));
            int & dv = g[v].degree;
            if(dv==1) num_d1--;
            else if(dv==2) num_d2--;
            //else bst.erase(node_info(v,dv));
            if(dv<=active_threshold) bst2.erase(node_info2(v,dv,g[v].score));
            for(int j=0; j<edge[v].size(); j++)
                if(check(v,j)) flag[edge[v][j]] = true;
            int old_dv = dv;
            for(int j=0; j<edge[w].size(); j++)
                if(check(w,j))
            {
                int k = edge[w][j];
                if(!flag[k])
                {
                    flag[k] = true;
                    if(g[k].degree==2) num_d2--;
                    else if(g[k].degree==1)
                    {
                        num_d1--;
                        num_d2++;
                    }
                    //g[k].degree can't be 0, as k is the neighbor of w
                    if(g[k].degree<=active_threshold) bst2.erase(node_info2(k,g[k].degree,g[k].score));
                    add_edge(v,k);
                    if(g[k].degree<=active_threshold) bst2.insert(node_info2(k,g[k].degree,g[k].score));
                    info.del_m--;
                }
            }
            for(int j=0; j<edge[v].size(); j++)
            {
                int k = edge[v][j];
                flag[k] = false;
                if(old_dv<=g[k].score && dv>old_dv)
                {
                    if(g[k].degree<=active_threshold) bst2.erase(node_info2(k,g[k].degree,g[k].score));
                    g[k].score = dv;
                    if(g[k].degree<=active_threshold) bst2.insert(node_info2(k,g[k].degree,g[k].score));
                }
            }

            if(dv==1) num_d1++;
            else if(dv==2) num_d2++;
            if(dv<=active_threshold) bst2.insert(node_info2(v,dv,g[v].score));
            if(dv>max_degree)
            {
                max_degree = dv;
                que.resize(max_degree);
            }
            if(dv!=old_dv && dv>0) que[dv-1].push(v);
            info.del_n += 2;    // u and w
            info.del_m += g[u].degree;
            del_node(u);
            info.del_m += g[w].degree;
            del_node(w);
            g[u].mis = true;
            misnum++;
            //if(g[v].degree>2) bst.insert(node_info(v,dv));
        }
    }

    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

alg_info degree_two_reduction_nogreedy()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "two";
    info.del_m = info.del_n = 0;

    while(!que[1].empty())
    {
        int u = que[1].front();
        que[1].pop();
        if(del[u] || g[u].degree!=2) continue;
        int i = 0;
        while(del[edge[u][i]]) i++;
        int v = edge[u][i];
        i++;
        while(del[edge[u][i]]) i++;
        int w = edge[u][i];
        if(g[v].degree<g[w].degree)
        {
            w = v;
            v = edge[u][i];
        }
        if(exist_edge(v,w))
        {
            info.del_n += 3;
            info.del_m += g[v].degree;
            del_node(v);
            info.del_m += g[w].degree;
            del_node(w);
            del[u] = true;
            --num_d0;
            edge[u].clear();
            g[u].mis = true;
            ++misnum;
        }
        else{
            merge_log.push_back(merge_type(u,v,w));
            int & dv = g[v].degree;
            if(dv==1) num_d1--;
            else if(dv==2) num_d2--;
            //else bst.erase(node_info(v,dv));
            //if(dv<=active_threshold) bst2.erase(node_info2(v,dv,g[v].score));
            for(int j=0; j<edge[v].size(); j++)
                if(check(v,j)) flag[edge[v][j]] = true;
            int old_dv = dv;
            for(int j=0; j<edge[w].size(); j++)
                if(check(w,j))
            {
                int k = edge[w][j];
                if(!flag[k])
                {
                    flag[k] = true;
                    if(g[k].degree==2) num_d2--;
                    else if(g[k].degree==1)
                    {
                        num_d1--;
                        num_d2++;
                    }
                    //g[k].degree can't be 0, as k is the neighbor of w
                    //if(g[k].degree<=active_threshold) bst2.erase(node_info2(k,g[k].degree,g[k].score));
                    add_edge(v,k);
                    //if(g[k].degree<=active_threshold) bst2.insert(node_info2(k,g[k].degree,g[k].score));
                    info.del_m--;
                }
            }
            for(int j=0; j<edge[v].size(); j++)
            {
                int k = edge[v][j];
                flag[k] = false;
                /*
                if(old_dv<=g[k].score && dv>old_dv)
                {
                    if(g[k].degree<=active_threshold) bst2.erase(node_info2(k,g[k].degree,g[k].score));
                    g[k].score = dv;
                    if(g[k].degree<=active_threshold) bst2.insert(node_info2(k,g[k].degree,g[k].score));
                }
                */
            }

            if(dv==1) num_d1++;
            else if(dv==2) num_d2++;
            //if(dv<=active_threshold) bst2.insert(node_info2(v,dv,g[v].score));
            if(dv>max_degree)
            {
                max_degree = dv;
                que.resize(max_degree);
            }
            if(dv!=old_dv && dv>0) que[dv-1].push(v);
            info.del_n += 2;    // u and w
            info.del_m += g[u].degree;
            del_node(u);
            info.del_m += g[w].degree;
            del_node(w);
            g[u].mis = true;
            misnum++;
            //if(g[v].degree>2) bst.insert(node_info(v,dv));
        }
    }

    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

void recheck_merge()
{
    for(int i=merge_log.size()-1; i>=0; i--)
    {
        merge_type tmp = merge_log[i];
        if(g[tmp.v].mis)
        {
            g[tmp.w].mis = true;
            g[tmp.u].mis = false;
        }
    }
}

bool dom_check(int u)
{
    bool dom = false;   //it is necessary to assign false to dom initially, to avoid reducing degree-0 vertices
    flag[u] = true;
    for(int i=0; i<edge[u].size(); i++)
        if(check(u,i)) flag[edge[u][i]] = true;
    for(int i=0; i<edge[u].size(); i++)
    {
        int v = edge[u][i];
        if(g[v].degree>g[u].degree) continue;
        dom = true;
        for(int j=0; j<edge[v].size(); j++)
            if(check(v,j) && !flag[edge[v][j]])
            {
                dom = false;
                break;
            }
        if(dom) break;
    }
    flag[u] = false;
    for(int i=0; i<edge[u].size(); i++)
        flag[edge[u][i]] = false;
    return dom;
}

alg_info brute_dom_reduction()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "dom";
    info.del_m = info.del_n = 0;
    for(int t=0; t<alive.size(); t++)
        if(check(t))
    {
        int u = alive[t];
        if(dom_check(u))
        {
            info.del_n++;
            info.del_m+=g[u].degree;
            del_node(u);
        }
    }
    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

double dom_sample(int sample_time)
{
    sample_time = min(sample_time, kernel_n / DOM_SAMPLE_NOM + 1);
    //sample_time = kernel_n / DOM_SAMPLE_NOM + 1;
    srand(time(NULL));
    int cnt = 0;
    int i = 0;
    while(i<sample_time && !alive.empty())
    {
        int t = rand() % alive.size();
        if(check(t))
        {
            i++;
            int u = alive[t];
            if(dom_check(u))
            {
                kernel_n--;
                kernel_m-=g[u].degree;
                del_node(u);
                cnt++;
            }
        }
    }
    return cnt * 1.0 / sample_time;
}

#include "Utility.h"

alg_info del_max_degree(bool batch)
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "del_max_degree";
    info.del_m = info.del_n = 0;
    long long edge_need_deleting = 0;
    if(batch) edge_need_deleting = kernel_m * INEXACT_DELM_NUM / INEXACT_DELM_NOM;

    while(max_degree>2)
    {
        if(que[max_degree-1].empty())
        {
            --max_degree;
            que.pop_back();
        }
        else
        {
            int u = que[max_degree-1].front();
            que[max_degree-1].pop();
            if(del[u] || g[u].degree!=max_degree) continue;
            info.del_n += 1;
            info.del_m += max_degree;
            del_node(u);
            chosen.push_back(u);
            if(info.del_m>=edge_need_deleting) break;
        }
    }
    /*
    while(!bst.empty())
    {
        int u = bst.begin()->id;
        int d = bst.begin()->degree;
        bst.erase(bst.begin());
        if(del[u]) continue;
        if(g[u].degree!=d)
        {
            if(g[u].degree>2) bst.insert(node_info(u,g[u].degree));
            continue;
        }
        info.del_n += 1;
        info.del_m += d;
        del_node(u);
        chosen.push_back(u);
        //break;
        if(info.del_m>=edge_need_deleting) break;
    }
    */
    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

bool bst2_LazyCheck(set<node_info2>::iterator iter)
{
    int u = iter->id;
    if(del[u])
    {
        bst2.erase(iter);
        return false;
    }
    //if(g[u].degree==0) return true;
    update_score(u);
    if(g[u].degree!=iter->degree || g[u].score>iter->score)
    {
        cerr << "Score2 Lazy Update ERROR!!!" << endl;
        cerr << u << ' ' << g[u].degree << ' ' << g[u].score << endl;

        cerr << iter->degree << ' ' << iter->score << endl;
        exit(0);
    }
    if(g[u].score<iter->score)  // need update
    {
        bst2.erase(iter);
        bst2.insert(node_info2(u,g[u].degree,g[u].score));
        return false;
    }
    else return true;   // pass the lazy check
}

alg_info add_min_degree(bool batch)
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "add_min_degree";
    info.del_m = info.del_n = 0;
    long long edge_need_deleting = 0;
    if(batch) edge_need_deleting = kernel_m * INEXACT_DELM_NUM / INEXACT_DELM_NOM;

    while(true)
    {
        while(bst2.empty() && active_threshold<max_degree)
        {
            int len = que[active_threshold].size();
            for(int i=0; i<len; ++i)
            {
                int u = que[active_threshold].front();
                que[active_threshold].pop();
                if(!del[u] && (g[u].degree==active_threshold+1))
                {
                    update_score(u);
                    bst2.insert(node_info2(u,g[u].degree,g[u].score));
                    que[active_threshold].push(u);
                }
            }
            ++active_threshold;
        }
        if(bst2.empty()) break;
        if(!bst2_LazyCheck(bst2.begin())) continue;
        int u = bst2.begin()->id;
        //bst2.erase(bst2.begin());
        for(int i=0; i<edge[u].size(); i++)
        {
            int v = edge[u][i];
            if(del[v]) continue;
            info.del_n++;
            info.del_m += g[v].degree;
            del_node(v);
        }
        ++info.del_n;
        del[u] = true;
        --num_d0;
        edge[u].clear();
        g[u].mis = true;
        ++misnum;
        chosen.push_back(u);
        if(info.del_m>=edge_need_deleting) break;
    }

    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

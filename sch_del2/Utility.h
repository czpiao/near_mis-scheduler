#pragma GCC optimize(3,"Ofast","inline")

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <random>
#include <map>
#include <set>
#include <iterator>

#define INEXACT_DELM_NUM 1
#define INEXACT_DELM_NOM 1000
#define INIT_EFFECT_THRESHOLD 100
#define EFFECT_THRESHOLD_NOM 1000
#define EXACT_ALG_NUM 3
#define GREEDY_ALG_NUM 2
#define INIT_SPEED 1000000.0
#define DOM_SAMPLE_TIMES 1000
#define DOM_SAMPLE_NOM 100

struct alg_info{
    string kind;
    int del_n;
    int del_m;
    double t;
};

struct merge_type{
    int u,v,w;
    merge_type(int a=0, int b=0, int c=0):u(a), v(b), w(c){}
};


struct simple_node_info{
    int id, degree;
    simple_node_info(int nid = 0, int ndegree = 0):id(nid), degree(ndegree){}
    void operator =(const simple_node_info &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
    }

    bool operator <(const simple_node_info &tmp) const  // smaller one: larger degree, larger score, smaller id
    {
        if(degree==tmp.degree) return(id<tmp.id);
        else return(degree>tmp.degree);
    }
};

struct del_node_info{
    int id, degree, score;
    del_node_info(int nid = 0, int ndegree = 0, int nscore = 0):id(nid), degree(ndegree), score(nscore){}
    void operator =(const del_node_info &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
        score = tmp.score;
    }

    bool operator <(const del_node_info &tmp) const  // smaller one: larger degree, larger score, smaller id
    {
        if(degree==tmp.degree)
        {
            if(score==tmp.score) return(id<tmp.id);
            else return(score>tmp.score);
        }
        else return(degree>tmp.degree);
    }
};

struct add_node_info{
    int id, degree, score;
    add_node_info(int nid = 0, int ndegree = 0, int nscore = 0):id(nid), degree(ndegree), score(nscore){}
    void operator =(const add_node_info &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
        score = tmp.score;
    }

    bool operator <(const add_node_info &tmp) const  // smaller one: smaller degree, larger score, smaller id
    {
        if(degree==tmp.degree)
        {
            if(score==tmp.score) return(id<tmp.id);
            else return(score>tmp.score);
        }
        else return(degree<tmp.degree);
    }
};

struct node_type{
    int degree = 0;
    int del_score = 0, add_score = 0;
    bool mis = false;
};

typedef vector<int> edge_type;

extern vector<node_type> g;
extern vector<edge_type> edge, backup_edge;

extern int n, kernel_n;
extern int m, kernel_m;
extern int misnum;

extern vector<int> alive;  // store the ids of alive vertices
extern vector<bool> del;   // identify which node is deleted during the peeling procedure
extern vector<bool> flag;

extern int num_d1, num_d2; // the exact number of degree-one and degree-two vertices
extern queue<int> que; // store the nodes(id) whose degree is one
extern queue<int> que2;    // store the nodes(id) whose degree is two
extern vector<int> chosen; // store the nodes chosen by inexact reduction
//extern set<node_info> bst;
extern set<del_node_info> del_bst;
extern set<add_node_info> add_bst;
extern vector<merge_type> merge_log;

//graph operation
inline void add_edge(int u, int v)
{
    edge[u].push_back(v);
    edge[v].push_back(u);
    ++g[u].degree;
    ++g[v].degree;
}

inline bool check(int u, int & i)   // remove edge[u][i] if it has been deleted
{
    if(del[edge[u][i]]){
        edge[u][i] = edge[u].back();
        edge[u].pop_back();
        --i;
        return false;
    }
    else return true;
}

inline bool check(int & i)   // remove alive[i] if it has been deleted
{
    if(del[alive[i]]){
        alive[i] = alive.back();
        alive.pop_back();
        --i;
        return false;
    }
    else return true;
}

inline bool exist_edge(int u, int v)
{
    if(g[u].degree<g[v].degree)
    {
        for(int i=0; i<edge[u].size(); ++i)
            if(check(u,i) && edge[u][i]==v) return true;
    }
    else
    {
        for(int i=0; i<edge[v].size(); ++i)
            if(check(v,i) && edge[v][i]==u) return true;
    }
    return false;
}

inline void become_degree_two(int u)
{
    ++num_d2;
    que2.push(u);
    for(int i=0; i<edge[u].size(); ++i)
        if(check(u,i))
        {
            int v = edge[u][i];
            ++g[v].del_score;
            //we need not erase the old value on del_bst at once
            if(g[v].degree>2) del_bst.insert(del_node_info(v,g[v].degree,g[v].del_score));
        }
}

inline void stop_being_degree_two(int u)
{
    --num_d2;
    for(int i=0; i<edge[u].size(); i++)
        if(check(u,i)) --g[edge[u][i]].del_score;
    // Lazy Update, needn't update del_bst
}

inline void degree_minus_one(int u)
{
    if(del[u]) return;
    if(g[u].degree==1) --num_d1;
    else if(g[u].degree==2)
    {
        ++num_d1;
        que.push(u);
        stop_being_degree_two(u);
    }
    else if(g[u].degree==3) become_degree_two(u);
    --g[u].degree;
}


void read_node_bin_data(string pathin);
void degree_minus_one(int u);
void del_node(int u);
void backup_all_edges();
void resume_all_edges();
void check_mis();

//debug
/*
void debug_output_node_info(int u);
void debug_output_nu_node_info(int u);
void debug_output_graph_info();
*/
/*
inline bool check_del_score(int u)
{
    int cnt = 0;
    for(int i=0; i<edge[u].size(); i++)
    {
        int v = edge[u][i];
        if(!del[v] && g[v].degree==2) ++cnt;
    }
    return (cnt==g[u].del_score);
}

inline bool check_nu_del_score(int u)
{
    if(!check_del_score(u)) return false;
    for(int i=0; i<edge[u].size(); i++)
    {
        int v = edge[u][i];
        if(del[v]) continue;
        if(!check_del_score(v)) return false;
    }
    return true;
}
*/
//reduction function
alg_info degree_one_reduction();
alg_info degree_two_reduction();
void recheck_merge();
bool dom_check(int u);
alg_info brute_dom_reduction();
double dom_sample(int sample_time);

//greedy function
alg_info del_max_degree(bool batch);
alg_info add_min_degree(bool batch);

//recover function
alg_info naive_recover();

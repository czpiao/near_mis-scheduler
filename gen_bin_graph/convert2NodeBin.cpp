/*
arguments setting
*.exe id
id in [0,30] represents edge type data
id > 30 represents node type data

NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
d is not the real degree, but the number of neighbors with larger id.
*/

#include <iostream>
using namespace std;
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>

int n, m;
int max_degree = 0;

vector<vector<int>> edge;

string InputPath;
string OutputPath;
bool edge_data = true;  // true : id in [0, 30]; false : id > 30

void add_edge(int u, int v)
{
    if(u<v) edge[u].push_back(v);
    else if(u>v) edge[v].push_back(u);
    else{
        cerr << "EDGE ERROR! u == v : " << u << endl;
        exit(0);
    }
}

void read_node_data()
{
    FILE *fp;
    if((fp=fopen(InputPath.c_str(),"r"))==NULL)
    {
        cerr << "can't open input file: " << InputPath << endl;
        exit(0);
    }
    fscanf(fp,"%d%d",&n,&m);
    edge.resize(n);
    for(int i=0; i<n; i++)
    {
        int v, d;
        fscanf(fp,"%d",&d);
        for(int j=0; j<d; j++)
        {
            fscanf(fp,"%d",&v);
            add_edge(i,v);
        }
    }
    fclose(fp);
}

void read_edge_data()
{
    FILE *fp;
    if((fp=fopen(InputPath.c_str(),"r"))==NULL)
    {
        cerr << "can't open input file: " << InputPath << endl;
        exit(0);
    }
    fscanf(fp,"%d%d",&n,&m);
    edge.resize(n);
    for(int i=0; i<m; i++)
    {
        int u, v;
        fscanf(fp,"%d%d",&u,&v);
        add_edge(u,v);
    }
    fclose(fp);
}

void sort_all_neighbor()
{
    for(int i=0; i<n; i++)
        sort(edge[i].begin(),edge[i].end());
}

void output_bin_graph()
{
    int data[2];
    data[0] = n;
    data[1] = m;

    int *degree = NULL;
    degree = (int*)malloc(sizeof(int)*n);
    for(int i=0; i<n; i++)
    {
        degree[i] = edge[i].size();
        max_degree = max(max_degree, degree[i]);
    }
    cerr << "max_degree = " << max_degree << endl;

    ofstream fout;
    fout.open(OutputPath.c_str(),ios::binary);
    fout.write((char*)data,sizeof(int)*2);
    int *buf = NULL;
    buf = (int*)malloc(sizeof(int)*n);
    for(int i=0; i<n; i++)
    {
        fout.write((char*)(degree+i),sizeof(int));
        for(int j=0; j<degree[i]; j++)
            buf[j] = edge[i][j];
        fout.write((char*)buf,sizeof(int)*degree[i]);
    }
    fout.close();
}

int str2int(char* s)
{
    int num = 0;
    int i = 0;
    while(s[i])
    {
        if(s[i]>'9' || s[i]<'0')
        {
            cerr << "WRONG ID: " << s[i] << endl;
            exit(0);
        }
        if(i>=9)
        {
            cerr << "id Length = " << i+1 << endl;
            exit(0);
        }
        num = num * 10 + (s[i] - '0');
        i++;
    }
    return num;
}

void gen_path(char* sid)
{
    int id = str2int(sid);
    if(id>30)   // node type data
    {
        edge_data = false;
        if(id<=37) InputPath = "ER_Graph\\";
        else if(id<=42) InputPath = "Bipartite_ER_Graph\\";
        else if(id<=48) InputPath = "Dense_ER_Graph\\";
        else{
            cerr << "WRONG ID: no such input file " << id << endl;
            exit(0);
        }
    }
    else InputPath = "Network\\";
    InputPath += sid;
    InputPath += ".in";
    OutputPath = "NodeBinGraph\\";
    OutputPath += sid;
    OutputPath += ".dat";
}

int main(int argc, char** argv)
{
    if(argc==2) gen_path(argv[1]);
    else{
        cerr << "ERROR: argc = " << argc << endl;
        exit(0);
    }
    if(edge_data) read_edge_data();
    else read_node_data();
    sort_all_neighbor();
    output_bin_graph();
    return 0;
}

#include "Utility.h"

alg_info del_max_degree(bool batch)
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "del_max_degree";
    info.del_m = info.del_n = 0;
    long long edge_need_deleting = 0;
    if(batch) edge_need_deleting = kernel_m * INEXACT_DELM_NUM / INEXACT_DELM_NOM;

    while(!bst.empty())
    {
        int u = bst.begin()->id;
        int d = bst.begin()->degree;
        bst.erase(bst.begin());
        if(del[u]) continue;
        if(g[u].degree!=d)
        {
            if(g[u].degree>2) bst.insert(node_info(u,g[u].degree));
            continue;
        }
        info.del_n += 1;
        info.del_m += d;
        del_node(u);
        chosen.push_back(u);
        //break;
        if(info.del_m>=edge_need_deleting) break;
    }
    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

alg_info add_min_degree()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "add_min_degree";

    return info;
}

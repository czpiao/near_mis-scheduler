#include "Utility.h"

/*
NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
d is not the real degree, but the number of neighbors with larger id.
*/
void read_node_bin_data(string pathin)
{
    FILE *fp;
    fp = fopen(pathin.c_str(), "rb");
    if(fp==NULL)
    {
        cerr << "can't open input file: " << pathin << endl;
        exit(0);
    }
    fread(&n,sizeof(int),1,fp);
    fread(&m,sizeof(int),1,fp);
    g.resize(n);
    edge.resize(n);
    int *buf = NULL;
    buf = (int*)malloc(sizeof(int)*n);
    for(int i=0; i<n; i++)
    {
        int d;
        fread(&d,sizeof(int),1,fp);
        fread(buf,sizeof(int),d,fp);
        for(int j=0; j<d; j++)
            add_edge(i,buf[j]);
    }
    free(buf);
    fclose(fp);
}

void del_node(int u)
{
    if(del[u]) return;
    del[u] = true;
    if(g[u].degree==1) num_d1--;
    else if(g[u].degree==2) stop_being_degree_two(u);
    for(int i=0; i<edge[u].size(); i++)
        degree_minus_one(edge[u][i]);
    edge[u].clear();
    g[u].degree = 0;
}

void backup_all_edges()
{
    backup_edge.resize(n);
    for(int i=0; i<n; i++)
        backup_edge[i] = edge[i];
}

void resume_all_edges()
{
    for(int i=0; i<n; i++)
        edge[i] = backup_edge[i];
    alive.clear();
    for(int i=0; i<n; i++)
    {
        g[i].degree = edge[i].size();
        del[i] = false;
        alive.push_back(i);
    }
    //g[i].mis and misnum stay unchanged.
}

void check_mis()
{
    int cnt = 0;
    for(int u=0; u<n; u++)
        if(g[u].mis)
    {
        cnt++;
        for(int i=0; i<edge[u].size(); i++)
        {
            int v = edge[u][i];
            if(g[v].mis)
            {
                cerr << "MIS CHECK ERROR: " << u << " and " << v << endl;
                exit(0);
            }
        }
    }
    if(cnt!=misnum)
    {
        cerr << "MIS CHECK ERROR: cnt != misnum" << endl;
        exit(0);
    }
    cerr << "Pass MIS check, #Near-MIS = " << misnum << endl;
}

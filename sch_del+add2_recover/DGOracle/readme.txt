input: graph G , an initail Near-MIS, the vertices need recovering 
output: a much larger Near-MIS

.\DGOracle_DGTwo.exe x

Here, x is the file id(1-48).
Read the grph G from ..\input\x.dat (a binary file).
Read the other infomation from ..\output\x.ans_log (a text file).
The answer log file(.ans_log) contains three parts.
	a. an initail Near-MIS
	b. the vertices chosen by greedy algorithms
	c. the history of merge operations(actually we do not use this part)
In each part, the first line shows the number of items. Then each line contains an specific item. 

DGOracle sonsists of two steps.

1. Construct index. 
	Run DGTwo algorithm supervised by the initial Near-MIS in Linear time.

2. Update.
	Try to recover the given vertices by valid swaps. DGOracle uses restricted dfs to find whether a valid swap exists. 
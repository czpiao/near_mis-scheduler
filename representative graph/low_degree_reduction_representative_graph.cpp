#pragma GCC optimize(3,"Ofast","inline")

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <map>
#include <set>
#include <iterator>
#include <list>

#define MAX_RECUR_NUM 5

struct alg_info{
    string kind;
    int kernel_n;
    int kernel_m;
    int misnum;
    int recur_num;
    double t;
};

struct node_type{
    bool mis;
    int id;
    list<int> rn;
    list<int> nbak;
    node_type()
    {
        mis = false;
    }

    inline int degree()
    {
        return rn.size();
    }

    inline int len()
    {
        return nbak.size();
    }

    inline void init();
    void touch();
    inline list<int>::iterator tag_node(list<int> & ll, list<int>::iterator it);
    void tag_list(list<int> & ll);
    void rtag_list(list<int> & ll);
    void del_node();
};

int n, m;
int max_degree;
vector<node_type> g;
int misnum;

vector<bool> del;
vector<int> d;
vector<bool> flag;
queue<int> que; // store the nodes(id) whose degree is one
queue<int> que2;    // store the nodes(id) whose degree is two

string InputPath, SchPath, AnsPath, RecordPath;

void add_edge(int u, int v)
{
    g[u].nbak.push_back(v);
    g[v].nbak.push_back(u);
}

/*
EDGE DATA FORMAT
n m
u0 v0
u1 v1
...
um-1 vm-1
*/
void read_edge_data()
{
    scanf("%d%d",&n,&m);
    g.resize(n);
    for(int i=0; i<m; i++)
    {
        int u, v;
        scanf("%d%d",&u,&v);
        add_edge(u,v);
    }
    for(int i=0; i<n; i++)
        g[i].id = i;
    max_degree = 0;
    for(int i=0; i<n; i++)
        max_degree = max(max_degree, g[i].len());
}

/*
NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
*/
void read_node_data()
{
    scanf("%d%d",&n,&m);
    g.resize(n);
    for(int i=0; i<n; i++)
    {
        int v, d;
        scanf("%d",&d);
        for(int j=0; j<d; j++)
        {
            scanf("%d",&v);
            add_edge(i,v);
        }
        g[i].id = i;
    }
    max_degree = 0;
    for(int i=0; i<n; i++)
        max_degree = max(max_degree, g[i].len());
}

/*
NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
d is not the real degree, but the number of neighbors with larger id.
*/
void read_node_bin_data(string pathin)
{
    FILE *fp;
    fp = fopen(pathin.c_str(), "rb");
    if(fp==NULL)
    {
        cerr << "can't open input file: " << pathin << endl;
        exit(0);
    }
    fread(&n,sizeof(int),1,fp);
    fread(&m,sizeof(int),1,fp);
    g.resize(n);
    for(int i=0; i<n; i++)
        g[i].id = i;
    //edge.resize(n);
    int *buf = NULL;
    buf = (int*)malloc(sizeof(int)*n);
    for(int i=0; i<n; i++)
    {
        int d;
        fread(&d,sizeof(int),1,fp);
        fread(buf,sizeof(int),d,fp);
        for(int j=0; j<d; j++)
            add_edge(i,buf[j]);
    }
    max_degree = 0;
    for(int i=0; i<n; i++)
        max_degree = max(max_degree, g[i].len());
    free(buf);
    fclose(fp);
}

bool contain(const list<int> & ll, int u)
{
    for(list<int>::const_iterator it=ll.begin(); it!=ll.end(); ++it)
        if((*it)==u) return true;
    return false;
}

inline void node_type::init()
{
    nbak.splice(nbak.begin(), rn);
    tag_list(nbak);
    rtag_list(nbak);
}

inline list<int>::iterator node_type::tag_node(list<int> & ll, list<int>::iterator it)
{
    if(del[*it] || flag[*it]) return ll.erase(it);  // remove deleted and duplicate elements
    else
    {
        flag[*it] = true;
        return ++it;    // return iterator always points to the next position
    }
}

void node_type::tag_list(list<int> & ll)  // remove deleted and duplicate elements and check self-loop while tagging
{
    list<int>::iterator it = ll.begin();
    while(it!=ll.end()) it = tag_node(ll, it);
}

void node_type::rtag_list(list<int> & ll)
{
    for(list<int>::iterator it=ll.begin(); it!=ll.end(); ++it)
        flag[*it] = false;
}

void node_type::touch()
{
    int old_d = rn.size();
    tag_list(rn);
    while(rn.size()<3 && !nbak.empty())
    {
        list<int>::iterator it = tag_node(nbak, nbak.begin());
        if(it!=nbak.begin()) rn.splice(rn.begin(), nbak, nbak.begin());
    }

        //if(tag_node(nbak, nbak.begin())!=nbak.begin()) rn.splice(rn.begin(), nbak, nbak.begin());
    rtag_list(rn);
    if(rn.size()<2 && old_d>=2) que.push(id);
    else if(rn.size()==2 && old_d>2) que2.push(id);
}

void node_type::del_node()   // delete this node
{
    init();
    del[id] = true;
    while(!nbak.empty())
    {
        int tmp = nbak.front();
        g[nbak.front()].touch();
        nbak.pop_front();
    }
}

void bdtwo_reduction()
{
    for(int i=0; i<n; i++)
        if(!del[i])
    {
        g[i].touch();
        int di = g[i].degree();
        if(di==0)
        {
            g[i].mis = true;
            misnum++;
            g[i].del_node();
        }
        else if(di==1) que.push(i);
        else if(di==2) que2.push(i);
    }

    while(true)
    {
        if(!que.empty())    //one(maybe zero) degree node exists
        {

            int u = que.front();
            que.pop();
            if(del[u]) continue;
            g[u].touch();
            if(g[u].degree()>1) continue;
            int v = -1;
            if(!g[u].rn.empty()) v = g[u].rn.front();
            g[u].mis = true;
            misnum++;
            g[u].del_node();
            if(v>=0) g[v].del_node();
        }
        else if(!que2.empty())
        {

            int u = que2.front();
            que2.pop();
            if(del[u]) continue;
            g[u].touch();
            if(g[u].degree()!=2) continue;
            int v = g[u].rn.front();
            int w = g[u].rn.back();
            if(g[w].len()>g[v].len())
            {
                int tmp = w;
                w = v;
                v = tmp;
            }   // make sure g[w].len is smaller
            g[u].mis = true;
            misnum++;
            g[u].del_node();
            if(contain(g[w].rn, v) || contain(g[w].nbak, v))
            {
                g[v].del_node();
                g[w].del_node();
            }
            else
            {
                g[w].init();
                del[w] = true;
                while(!g[w].nbak.empty())
                {
                    int x = g[w].nbak.front();
                    g[w].nbak.pop_front();
                    add_edge(v, x);
                    g[x].touch();
                }
                g[v].touch();
            }
        }
        else break;
    }
    for(int u=0; u<n; u++)
        if(!del[u]) g[u].init();
}

inline void simple_del_node(int u)
{
    del[u] = true;
    for(list<int>::iterator it=g[u].nbak.begin(); it!=g[u].nbak.end(); ++it)
        d[*it]--;
    //g[u].nbak.clear();
}

bool quick_dom_reduction()
{
    bool DelAnyNode = false; // whether delete any node

    vector<vector<int>> node_bucket;
    int max_degree = 0;

    for(int i=0; i<n; i++)
        if(!del[i])
    {
        d[i] = g[i].len();
        max_degree = max(max_degree, d[i]);
    }

    node_bucket.resize(max_degree+1);

    for(int i=0; i<n; i++)
        if(!del[i]) node_bucket[d[i]].push_back(i);

    for(int i=max_degree; i>0; i--)
        for(int j=0; j<node_bucket[i].size(); j++)
    {
        int u = node_bucket[i][j];
        if(del[u]) continue;
        if(d[u]<i)
        {
            node_bucket[d[u]].push_back(u);
            continue;
        }

        flag[u] = true;
        list<int>::iterator it = g[u].nbak.begin();
        while(it!=g[u].nbak.end())
        {
            if(del[*it]) it = g[u].nbak.erase(it);
            else
            {
                flag[*it] = true;
                ++it;
            }
        }

        bool domu = false;
        for(it=g[u].nbak.begin(); it!=g[u].nbak.end(); ++it)

            if(d[*it]<=d[u])
        {
            domu = true;
            list<int>::iterator it2 = g[*it].nbak.begin();
            while(it2!=g[*it].nbak.end())
            {
                if(del[*it2]) it2 = g[*it].nbak.erase(it2);
                else if(!flag[*it2])
                {
                    domu = false;
                    break;
                }
                else ++it2;
            }
            if(domu) break;
        }

        flag[u] = false;
        for(it=g[u].nbak.begin(); it!=g[u].nbak.end(); ++it)
            flag[*it] = false;

        if(domu)    // delete node u
        {
            DelAnyNode = true;
            simple_del_node(u);
        }
    }

    for(int j=0; j<node_bucket[0].size(); j++)
    {
        int u = node_bucket[0][j];
        if(!del[u])
        {
            g[u].mis = true;
            misnum++;
            del[u] = true;
            DelAnyNode = true;
        }
    }

    return DelAnyNode;
}

alg_info kernel()
{
    clock_t startTime,endTime;
    startTime = clock();

    del.resize(n, false);
    flag.resize(n, false);
    d.resize(n, 0);

    int recur_num = 0;
    bdtwo_reduction();
    /*
    for(recur_num=1; recur_num<=MAX_RECUR_NUM; recur_num++)
    {
        clock_t time0, time1, time2;
        time0 = clock();
        bdtwo_reduction();
        time1 = clock();
        cerr<< "time0 =\t" << (double)(time1 - time0) / CLOCKS_PER_SEC << endl;
        cerr<< "|MIS| =\t" << misnum << endl;
        if(!quick_dom_reduction()) break;
        time2 = clock();
        cerr<< "time1 =\t" << (double)(time2 - time1) / CLOCKS_PER_SEC << endl;
        cerr<< "|MIS| =\t" << misnum << endl;
    }
    */

    alg_info tmp;
    tmp.kind = "Representative_Graph";
    tmp.misnum = misnum;
    tmp.recur_num = recur_num;
    endTime = clock();
    tmp.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    cerr << "time =\t" << tmp.t << endl;
    cerr << "|MIS| =\t" << misnum << endl;
    return tmp;
}

void output_info(alg_info tmp)
{
    ofstream fout;
    fout.open(RecordPath.c_str(),ios::app);
    fout << tmp.kind << "\t" << tmp.t << '\t' << tmp.misnum << '\t' << n << '\t' << m << '\t' << tmp.kernel_n << '\t' << tmp.kernel_m << endl;
    fout.close();
}

void output_ana(alg_info tmp)
{
    ofstream fout;
    fout.open("record.txt",ios::app);
    fout << tmp.kind << "\t" << n << '\t' << m << '\t' << max_degree << '\t';
    fout << tmp.t << '\t' << tmp.recur_num << '\t' << tmp.misnum << '\t' << tmp.kernel_n << '\t' << tmp.kernel_m << '\t';
    fout << endl;
    fout.close();
}


void output_kernel(alg_info & tmp)    // output the kernel graph in node type
{
    int kn = 0;
    int km = 0;
    vector<int> kernel_id;
    kernel_id.resize(n);
    for(int i=0; i<n; i++)
    {
        if(!del[i])
        {
            kernel_id[i] = kn;
            kn++;
            km += g[i].len();
        }
        else kernel_id[i] = -1;
    }
    tmp.kernel_n = kn;
    tmp.kernel_m = km / 2;
    cerr << "kernel_n =\t" << tmp.kernel_n << " / " << n << endl;
    cerr << "kernel_m =\t" << tmp.kernel_m << " / " << m << endl;
    //printf("%d %lld\n",tmp.kernel_n,tmp.kernel_m);
    /*
    for(int i=0; i<n; i++)
        if(!del[i])
    {
        vector<int> nei;
        for(list<int>::iterator it=g[i].nbak.begin(); it!=g[i].nbak.end(); ++it)
            if(!del[*it] && (*it)>i) nei.push_back(*it);
        printf("%d", nei.size());
        for(int j=0; j<nei.size(); j++)
        {
            int v = nei[j];
            if(del[v]||v<=i)
            {
                cerr << "Kernel Graph Error!" << endl;
                exit(0);
            }
            printf(" %d", kernel_id[v]);
        }
        printf("\n");
    }
    */
}

void gen_path(char* sid)
{
    InputPath = "input\\";
    InputPath += sid;
    InputPath += ".dat";

    SchPath = "output\\";
    SchPath += sid;
    AnsPath = SchPath + ".ans_log";
    SchPath += ".sch_log";

    RecordPath = "output\\record.txt";
}

int main(int argc, char** argv)
{
    alg_info tmp;
    if(argc!=2)
    {
        cerr << "ERROR: argc = " << argc << endl;
        exit(0);
    }
    else gen_path(argv[1]);
    read_node_bin_data(InputPath);
    //read_node_data();
    //read_edge_data();
    tmp = kernel();
    output_kernel(tmp);
    output_info(tmp);
    //output_ana(tmp);
    return 0;
}

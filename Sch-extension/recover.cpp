#include "Utility.h"

alg_info naive_recover()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "naive_recover";
    info.del_n = 0;    // Here, del_n represents the number of recovered vertices.

    vector<node_info> candidate;
    for(int i=0; i<chosen.size(); i++)
    {
        int u = chosen[i];
        if(!g[u].mis) candidate.push_back(node_info(u,g[u].degree));
    }
    sort(candidate.begin(),candidate.end());
    for(int i=candidate.size()-1; i>=0; i--)
    {
        int u = candidate[i].id;
        bool ok = true;
        for(int j=0; j<edge[u].size(); j++)
            if(g[edge[u][j]].mis)
            {
                ok = false;
                break;
            }
        if(ok)
        {
            g[u].mis = true;
            info.del_n++;
        }
    }
    misnum += info.del_n;

    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

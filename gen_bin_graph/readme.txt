Convert edge type data and node type data to binary node type files.

Network stores edge type data. 0.in ... 30.in

EDGE DATA FORMAT
n m
u0 v0
u1 v1
...
um-1 vm-1

ER_Graph stores node type data. 31.in ... 37.in

NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

Each edge exists once.
d is not the real degree, but the number of neighbors with larger id.
No self loop exists.

Bipartite_ER_Graph stores node type data. 38.in ... 42.in
Dense_ER_Graph stores node type data. 43.in ... 48.in

Arguments Setting:
convert2NodeBin.exe id

Finally output the binary node type graphs into NodeBinGraph. 0.dat ... 48.dat

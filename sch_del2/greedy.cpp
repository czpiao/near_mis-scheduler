#include "Utility.h"

alg_info del_max_degree(bool batch)
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "del_max_degree_2.0";
    info.del_m = info.del_n = 0;
    long long edge_need_deleting = 0;
    if(batch) edge_need_deleting = kernel_m * INEXACT_DELM_NUM / INEXACT_DELM_NOM;

    while(!del_bst.empty())
    {
        int u = del_bst.begin()->id;
        int d = del_bst.begin()->degree;
        int s = del_bst.begin()->score;
        del_bst.erase(del_bst.begin());
        if(del[u]) continue;
        //check whether degree and del_score are the latest
        if(g[u].degree!=d || g[u].del_score!=s)
        {
            if(g[u].degree>2) del_bst.insert(del_node_info(u,g[u].degree,g[u].del_score));
            continue;
        }
        info.del_n++;
        info.del_m += g[u].degree;
        //cerr << u << '\t' << g[u].degree << '\t' << g[u].del_score << endl;
        del_node(u);
        chosen.push_back(u);
        if(info.del_m>=edge_need_deleting) break;
    }
    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    return info;
}

alg_info add_min_degree(bool batch = true)
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info info;
    info.kind = "add_min_degree";
    info.del_m = info.del_n = 0;
    long long edge_need_deleting = 0;
    if(batch) edge_need_deleting = kernel_m * INEXACT_DELM_NUM / INEXACT_DELM_NOM;
    /*
    while(!bst.empty())
    {
        set<node_info>::iterator it = bst.end();
        --it;
        int u = it->id;
        int d = it->degree;
        bst.erase(it);
        if(del[u]) continue;
        if(g[u].degree!=d)
        {
            if(g[u].degree>2) bst.insert(node_info(u,g[u].degree));
            continue;
        }
        for(int i=0; i<edge[u].size(); i++)
        {
            int v = edge[u][i];
            if(del[v]) continue;
            info.del_n++;
            info.del_m += g[v].degree;
            del_node(v);
        }
        g[u].mis = true;
        misnum++;
        info.del_n++;
        del_node(u);
        chosen.push_back(u);
        if(info.del_m>=edge_need_deleting) break;
    }
    */
    endTime = clock();
    info.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;

    return info;
}

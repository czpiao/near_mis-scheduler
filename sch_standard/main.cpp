#include "Utility.h"

vector<node_type> g;
vector<edge_type> edge, backup_edge;

int n, kernel_n;
int m, kernel_m;
int misnum = 0;

vector<int> alive;
vector<bool> del;
vector<bool> flag;

int num_d1, num_d2;
queue<int> que, que2;
vector<int> chosen;
vector<merge_type> merge_log;
set<node_info> bst;

vector<alg_info> sch_log;

double est_time[EXACT_ALG_NUM];
int est_node[EXACT_ALG_NUM];
int est_effect[EXACT_ALG_NUM];  // est_node / est_time
int effect_threshold;   // #deleted nodes / time(ms)
double est_speed[EXACT_ALG_NUM];  // |E| / time,
int est_error_num = 0;

string InputPath, SchPath, AnsPath, RecordPath;

void apply_greedy(int i)
{
    alg_info info;
    if(i==0) info = del_max_degree(true);
    else if(i==1) info = add_min_degree();
    else{
        cerr << "Wrong Greedy ALG ID: " << i << endl;
        exit(0);
    }
    kernel_n -= info.del_n;
    kernel_m -= info.del_m;
    sch_log.push_back(info);
}

int choose_greedy()
{
    return 0;
}

void apply_reduction(int i)
{
    alg_info info;
    if(i==0) info = degree_one_reduction();
    else if(i==1) info = degree_two_reduction();
    else if(i==2) info = brute_dom_reduction();
    else{
        cerr << "Wrong Reduction ALG ID: " << i << endl;
        exit(0);
    }
    est_speed[i] = kernel_m / (info.t * 1000 + 1);
    kernel_n -= info.del_n;
    kernel_m -= info.del_m;
    sch_log.push_back(info);
    if(i<EXACT_ALG_NUM && (info.del_n/int(info.t*1000+1)<effect_threshold))  // if the effect is worse than estimation, then apply greedy at once
    {
        est_error_num++;
        apply_greedy(choose_greedy());
    }
}

int estimate_best_reduction()
{
    est_node[0] = num_d1;
    est_node[1] = num_d2;
    est_node[2] = int(kernel_n * dom_sample(DOM_SAMPLE_TIMES));
    for(int i=0; i<EXACT_ALG_NUM; i++)
    {
        est_time[i] = kernel_m / est_speed[i]; //ms
        est_effect[i] = est_node[i] / int(est_time[i] + 1);   //nodes per ms
    }
    int best_reduction_id = 0;
    for(int i=1; i<EXACT_ALG_NUM; i++)
        if(est_effect[i]>=est_effect[best_reduction_id])
            best_reduction_id = i;
    return best_reduction_id;
}

alg_info sch()
{
    clock_t startTime,endTime;
    startTime = clock();
    alg_info tmp;
    tmp.kind = "sch_standard(batch)";

    del.resize(n, false);
    flag.resize(n, false);
    num_d1 = num_d2 = 0;
    for(int i=0; i<n; i++)
    {
        if(g[i].degree==0)
        {
            del[i] = true;
            g[i].mis = true;
            misnum++;
        }
        else if(g[i].degree==1)
        {
            que.push(i);
            num_d1++;
            alive.push_back(i);
        }
        else if(g[i].degree==2)
        {
            que2.push(i);
            num_d2++;
            alive.push_back(i);
        }
        else{
            bst.insert(node_info(i,g[i].degree));
            alive.push_back(i);
        }
    }
    kernel_n = alive.size();
    kernel_m = m;
    for(int i=0; i<EXACT_ALG_NUM; i++)
        est_speed[i] = INIT_SPEED;

    while(kernel_m>0)
    {
        effect_threshold = min(INIT_EFFECT_THRESHOLD, kernel_n/EFFECT_THRESHOLD_NOM+1);
        int reduction_id = estimate_best_reduction();
        // bst is empty means that max degree <= 2, then do not use greedy algorithm
        if(bst.empty() || est_effect[reduction_id]>=effect_threshold) apply_reduction(reduction_id);
        else apply_greedy(choose_greedy());
    }

    for(int t=0; t<alive.size(); t++)
        if(check(t))
        {
            int u = alive[t];
            del[u] = g[u].mis = true;
            misnum++;
        }

    endTime = clock();
    tmp.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    cerr << "tot_time =\t" << tmp.t << endl;
    cerr << "misnum =\t" << misnum << endl;
    return tmp;
}

void output_record(alg_info tmp, alg_info beta)
{
    ofstream fout;
    fout.open(RecordPath.c_str(),ios::app);
    fout << tmp.t + beta.t  << '\t' << misnum << '\t' << tmp.kind << '\t' << beta.kind << '\t' << beta.t << '\t' << beta.del_n << endl;
}


void output_sch_log(alg_info tmp)
{
    ofstream fout;
    fout.open(SchPath.c_str());
    fout << tmp.kind << '\t' << tmp.t << '\t' << misnum << '\t' << sch_log.size() << '\t' << est_error_num << endl;
    for(int i=0; i<sch_log.size(); i++)
    {
        fout << sch_log[i].kind << '\t' << sch_log[i].t << '\t' << sch_log[i].del_n << '\t' << sch_log[i].del_m << endl;
    }
    fout.close();
}

void output_ans_log()
{
    ofstream fout;
    fout.open(AnsPath.c_str());
    cerr << "misnum, |chosen|, |merge_log|:\t" << misnum << '\t' << chosen.size() << '\t' << merge_log.size() << endl;
    fout << misnum << endl;
    int cnt = 0;
    for(int i=0; i<n; i++)
        if(g[i].mis)
    {
        cnt++;
        fout << i << endl;
    }
    if(cnt!=misnum) cerr << "misnum error" << endl;
    fout << chosen.size() << endl;
    for(int i=0; i<chosen.size(); i++)
        fout << chosen[i] << endl;
    fout << merge_log.size() << endl;
    for(int i=0; i<merge_log.size(); i++)
        fout << merge_log[i].u << '\t' << merge_log[i].v << '\t' << merge_log[i].w << endl;
    fout.close();
}

void gen_path(char* sid)
{
    InputPath = "input\\";
    InputPath += sid;
    InputPath += ".dat";

    SchPath = "output\\";
    SchPath += sid;
    AnsPath = SchPath + ".ans_log";
    SchPath += ".sch_log";

    RecordPath = "output\\record.txt";
}

int main(int argc, char** argv)
{
    alg_info tmp, beta;
    if(argc!=2)
    {
        cerr << "ERROR: argc = " << argc << endl;
        exit(0);
    }
    else gen_path(argv[1]);
    read_node_bin_data(InputPath);
    backup_all_edges();
    tmp = sch();
    recheck_merge();
    resume_all_edges();
    beta = naive_recover();
    check_mis();
    output_record(tmp,beta);
    output_sch_log(tmp);
    //output_ans_log();
    return 0;
}

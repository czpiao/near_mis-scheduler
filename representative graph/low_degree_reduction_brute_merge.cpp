#pragma GCC optimize(3,"Ofast","inline")

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <map>
#include <set>
#include <iterator>

#define MAX_RECUR_NUM 5


struct alg_info{
    string kind;
    int kernel_n;
    int kernel_m;
    int misnum;
    int recur_num;
    double t;
};

struct node_type{
    int degree;
    bool mis;
};

typedef vector<int> edge_type;

struct node_info{
    int id,degree;
    node_info(int nid = 0, int ndegree = 0):id(nid), degree(ndegree){}
    void operator =(const node_info &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
    }

    bool operator <(const node_info &tmp) const  // smaller one: larger degree, smaller id
    {
        if(degree==tmp.degree) return(id<tmp.id);
        else return(degree>tmp.degree);
    }
};

struct degree2path{
    bool VirtualEdge;   // true: we add an virtual edge (u, v)
    int u, v;
    vector<int> node;   // u - node[0] - ... node[size-1] - v
};


int n;
int m;
vector<node_type> g;
vector<edge_type> edge;
vector<edge_type> dual;     // edge[u][i]==v && edge[v][j]==u -> dual[u][i]==j && dual[v][j]==i
int misnum = 0;
vector<int> chosen; // store the nodes chosen by inexact reduction
vector<degree2path> path;

vector<bool> flag;

vector<bool> del;   // identify which node is deleted during the peeling procedure
queue<int> que; // store the nodes(id) whose degree is one
queue<int> que2;    // store the nodes(id) whose degree is two

string InputPath, SchPath, AnsPath, RecordPath;

void add_edge(int u, int v)
{
    edge[u].push_back(v);
    edge[v].push_back(u);
    dual[u].push_back(g[v].degree);
    dual[v].push_back(g[u].degree);
    g[u].degree++;
    g[v].degree++;
    if(g[u].degree<edge[u].size())
    {
        int p = g[u].degree - 1;
        int q = edge[u].size() - 1;
        swap(edge[u][p], edge[u][q]);
        swap(dual[u][p], dual[u][q]);
        dual[edge[u][q]][dual[u][q]] = q;
    }
    if(g[v].degree<edge[v].size())
    {
        int p = g[v].degree - 1;
        int q = edge[v].size() - 1;
        swap(edge[v][p], edge[v][q]);
        swap(dual[v][p], dual[v][q]);
        dual[edge[v][q]][dual[v][q]] = q;
    }
}

bool exist_edge(int u, int v)
{
    if(g[u].degree<g[v].degree)
    {
        for(int i=0; i<g[u].degree; i++)
            if(edge[u][i]==v) return true;
    }
    else
    {
        for(int i=0; i<g[v].degree; i++)
            if(edge[v][i]==u) return true;
    }
    return false;
}

/*
EDGE DATA FORMAT
n m
u0 v0
u1 v1
...
um-1 vm-1
*/
void read_edge_data()
{
    scanf("%d%d",&n,&m);
    g.resize(n);
    edge.resize(n);
    dual.resize(n);
    for(int i=0; i<n; i++)
        g[i].mis = false;
    for(int i=0; i<m; i++)
    {
        int u, v;
        scanf("%d%d",&u,&v);
        add_edge(u,v);
    }
}

/*
NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
*/
void read_node_data()
{
    scanf("%d%d",&n,&m);
    g.resize(n);
    edge.resize(n);
    dual.resize(n);
    for(int i=0; i<n; i++)
        g[i].mis = false;
    for(int i=0; i<n; i++)
    {
        int v, d;
        scanf("%d",&d);
        for(int j=0; j<d; j++)
        {
            scanf("%d",&v);
            add_edge(i,v);
        }
    }
}


/*
NODE DATA FORMAT
n m
d v0 v1 v2 ... vd-1
...

each edge exists once
d is not the real degree, but the number of neighbors with larger id.
*/
void read_node_bin_data(string pathin)
{
    FILE *fp;
    fp = fopen(pathin.c_str(), "rb");
    if(fp==NULL)
    {
        cerr << "can't open input file: " << pathin << endl;
        exit(0);
    }
    fread(&n,sizeof(int),1,fp);
    fread(&m,sizeof(int),1,fp);
    g.resize(n);
    for(int i=0; i<n; i++)
    {
        g[i].mis = false;
        g[i].degree = 0;
    }
    edge.resize(n);
    dual.resize(n);
    int *buf = NULL;
    buf = (int*)malloc(sizeof(int)*n);
    for(int i=0; i<n; i++)
    {
        int d;
        fread(&d,sizeof(int),1,fp);
        fread(buf,sizeof(int),d,fp);
        for(int j=0; j<d; j++)
            add_edge(i,buf[j]);
    }
    free(buf);
    fclose(fp);
}


void del_node(int u)
{
    for(int i=0; i<g[u].degree; i++)
    {
        int v = edge[u][i];
        int j = dual[u][i]; // edge[v][j]==u
        int & dv = g[v].degree;
        dv--;
        swap(edge[v][j],edge[v][dv]);   // in the adjacency list of v, swap u and the last alive neighbor
        swap(dual[v][j],dual[v][dv]);
        dual[u][i] = dv;
        dual[edge[v][j]][dual[v][j]] = j;
        if(dv==1) que.push(v);
        else if(dv==2) que2.push(v);
    }
    g[u].degree = 0;
    del[u] = true;
}

void del_node_and_neighbors(int u) // delete the node u and all its neighbors
{
    while(g[u].degree>0) del_node(edge[u][0]);
    del[u] = true;
}

bool quick_dom_reduction()
{
    bool DelAnyNode = false; // whether delete any node

    vector<vector<int>> node_bucket;
    int max_degree = 0;
    vector<bool> vis;

    for(int i=0; i<n; i++)
        if(!del[i]) max_degree = max(max_degree, g[i].degree);
    node_bucket.resize(max_degree+1);
    for(int i=0; i<n; i++)
        if(!del[i]) node_bucket[g[i].degree].push_back(i);

    for(int i=max_degree; i>0; i--)
        for(int j=0; j<node_bucket[i].size(); j++)
    {
        int u = node_bucket[i][j];
        if(del[u]) continue;
        int du = g[u].degree;
        if(du<i)
        {
            node_bucket[du].push_back(u);
            continue;
        }
        for(int k=0; k<du; k++)    // mark u's neighbors and u
            flag[edge[u][k]] = true;
        flag[u] = true;
        bool domu = false;
        for(int k=0; k<du; k++)
        {
            int v = edge[u][k];
            int dv = g[v].degree;
            if(dv<=du)
            {
                domu = true;
                for(int k2=0; k2<dv; k2++)
                    if(!flag[edge[v][k2]])
                {
                    domu = false;
                    break;
                }
                if(domu) break;
            }
        }
        for(int k=0; k<du; k++)    // unmark u's neighbors and u
            flag[edge[u][k]] = false;
        flag[u] = false;
        if(domu)    // delete node u
        {
            DelAnyNode = true;
            for(int k=0; k<du; k++)
            {
                int v = edge[u][k];
                int k2 = dual[u][k];
                int & dv = g[v].degree;
                dv--;
                swap(edge[v][k2],edge[v][dv]);   // in the adjacency list of v, swap u and the last alive neighbor
                swap(dual[v][k2],dual[v][dv]);
                dual[u][k] = dv;
                dual[edge[v][k2]][dual[v][k2]] = k2;
                if(dv==0) node_bucket[0].push_back(v);
            }
            g[u].degree = 0;
            del[u] = true;
        }
    }
    for(int j=0; j<node_bucket[0].size(); j++)
    {
        int u = node_bucket[0][j];
        if(!del[u])
        {
            g[u].mis = true;
            misnum++;
            del[u] = true;
            DelAnyNode = true;
        }
    }
    return DelAnyNode;
}

void bdtwo_reduction()
{
    for(int i=0; i<n; i++)
        if(!del[i])
    {
        if(g[i].degree==0)
        {
            g[i].mis = true;
            misnum++;
            del[i] = true;
            continue;
        }
        if(g[i].degree==1) que.push(i);
        else if(g[i].degree==2) que2.push(i);
    }
    while(true)
    {
        if(!que.empty())    //one(maybe zero) degree node exists
        {
            int u = que.front();
            que.pop();
            if(del[u] || g[u].degree>1) continue;
            g[u].mis = true;
            misnum++;
            del_node_and_neighbors(u);
        }
        else if(!que2.empty())
        {
            int u = que2.front();
            que2.pop();
            if(del[u] || g[u].degree!=2) continue;
            int v = edge[u][0];
            int w = edge[u][1];
            if(exist_edge(v,w))
            {
                g[u].mis = true;
                misnum++;
                del_node_and_neighbors(u);
            }
            else
            {
                n++;
                g.resize(n);
                edge.resize(n);
                dual.resize(n);
                g[n-1].mis = false;
                g[n-1].degree = 0;
                del.push_back(false);
                flag.push_back(false);
                for(int i=0; i<g[v].degree; i++)
                {
                    int k = edge[v][i];
                    flag[k] = true;
                    if(k!=u) add_edge(k, n-1);
                }
                for(int i=0; i<g[w].degree; i++)
                {
                    int k = edge[w][i];
                    if(!flag[k]) add_edge(k, n-1);
                }
                for(int i=0; i<g[v].degree; i++)
                    flag[edge[v][i]] = false;
                g[u].mis = true;
                misnum++;
                del_node_and_neighbors(u);
            }
        }
        else break;
    }
}

alg_info kernel()
{
    clock_t startTime,endTime;
    startTime = clock();

    del.resize(n, false);
    flag.resize(n, false);

    int recur_num = 0;

    bdtwo_reduction();

    /*
    for(recur_num=1; recur_num<=MAX_RECUR_NUM; recur_num++)
    {
        bdtwo_reduction();
        if(!quick_dom_reduction()) break;
    }
    */

    /*
    for(recur_num=1; recur_num<=MAX_RECUR_NUM; recur_num++)
    {
        clock_t time0, time1, time2;
        time0 = clock();
        bdtwo_reduction();
        time1 = clock();
        cerr<< "time0 =\t" << (double)(time1 - time0) / CLOCKS_PER_SEC << endl;
        cerr<< "|MIS| =\t" << misnum << endl;
        if(!quick_dom_reduction()) break;
        time2 = clock();
        cerr<< "time1 =\t" << (double)(time2 - time1) / CLOCKS_PER_SEC << endl;
        cerr<< "|MIS| =\t" << misnum << endl;
    }
    */

    alg_info tmp;
    tmp.kind = "brute_merge";
    tmp.misnum = misnum;
    tmp.recur_num = recur_num;
    endTime = clock();
    tmp.t = (double)(endTime - startTime) / CLOCKS_PER_SEC;
    cerr << "time =\t" << tmp.t << endl;
    cerr << "|MIS| =\t" << misnum << endl;
    return tmp;
}

void check()
{
    int num = 0;
    for(int i=0; i<n; i++)
    {
        if(g[i].degree==0 && !del[i]) cerr << "Node Deletion Error!" << endl;
        g[i].degree = edge[i].size();
        if(g[i].mis)
        {
            num++;
            for(int j=0; j<g[i].degree; j++)
            {
                int v = edge[i][j];
                if(g[v].mis)
                {
                    cerr << "IS CHECK: NO" << endl;
                    cerr << i << "\t" << v << "\tconflict" << endl;
                    return;
                }
            }
        }
    }
    if(num!=misnum)
    {
        cerr << "IS CHECK: NO" << endl;
        cerr << "real misnum =\t" << num << "\t!!!" << endl;
        return;
    }
    cerr << "IS CHECK: YES" << endl;
    cerr << "|MIS| =\t" << num << endl;
}

void output_info(alg_info tmp)
{
    ofstream fout;
    fout.open(RecordPath.c_str(),ios::app);
    fout << tmp.kind << "\t" << tmp.t << '\t' << tmp.misnum << '\t' << n << '\t' << m << '\t' << tmp.kernel_n << '\t' << tmp.kernel_m << endl;
    fout.close();
}

void output_ana(alg_info tmp)
{
    ofstream fout;
    fout.open("record.txt",ios::app);
    fout << tmp.kind << "\t" << n << '\t' << m << '\t';
    fout << tmp.t << '\t' << tmp.recur_num << '\t' << tmp.misnum << '\t' << tmp.kernel_n << '\t' << tmp.kernel_m << '\t';
    fout << endl;
    fout.close();
}

void output_kernel(alg_info & tmp)    // output the kernel graph in node type
{
    int kn = 0;
    int km = 0;
    vector<int> kernel_id;
    kernel_id.resize(n);
    for(int i=0; i<n; i++)
    {
        if(!del[i])
        {
            kernel_id[i] = kn;
            kn++;
            km += g[i].degree;
        }
        else kernel_id[i] = -1;
    }
    tmp.kernel_n = kn;
    tmp.kernel_m = km / 2;
    cerr << "kernel_n =\t" << tmp.kernel_n << " / " << n << endl;
    cerr << "kernel_m =\t" << tmp.kernel_m << " / " << m << endl;
    //printf("%d %lld\n",tmp.kernel_n,tmp.kernel_m);
    /*
    for(int i=0; i<n; i++)
        if(!del[i])
    {
        vector<int> nei;
        for(int j=0; j<g[i].degree; j++)
            if(edge[i][j]>i) nei.push_back(edge[i][j]);
        printf("%d", nei.size());
        for(int j=0; j<nei.size(); j++)
        {
            int v = nei[j];
            if(del[v]||v<=i)
            {
                cerr << "Kernel Graph Error!" << endl;
                exit(0);
            }
            printf(" %d", kernel_id[v]);
        }
        printf("\n");
    }
    */
}

void gen_path(char* sid)
{
    InputPath = "input\\";
    InputPath += sid;
    InputPath += ".dat";

    SchPath = "output\\";
    SchPath += sid;
    AnsPath = SchPath + ".ans_log";
    SchPath += ".sch_log";

    RecordPath = "output\\record.txt";
}

int main(int argc, char** argv)
{
    alg_info tmp;
    if(argc!=2)
    {
        cerr << "ERROR: argc = " << argc << endl;
        exit(0);
    }
    else gen_path(argv[1]);
    read_node_bin_data(InputPath);
    //read_node_data();
    //read_edge_data();
    tmp = kernel();
    output_kernel(tmp);
    output_info(tmp);
    //check();
    //output_ana(tmp);
    return 0;
}

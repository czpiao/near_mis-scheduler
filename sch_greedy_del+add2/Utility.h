#pragma GCC optimize(3,"Ofast","inline")

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <random>
#include <map>
#include <set>
#include <iterator>

#define INEXACT_DELM_NUM 1
#define INEXACT_DELM_NOM 1000
#define INIT_EFFECT_THRESHOLD 100
#define EFFECT_THRESHOLD_NOM 1000
#define EXACT_ALG_NUM 3
#define GREEDY_ALG_NUM 2
#define INIT_SPEED 1000000.0
#define DOM_SAMPLE_TIMES 1000
#define DOM_SAMPLE_NOM 100

struct alg_info{
    string kind;
    int del_n;
    int del_m;
    double t;
};

struct merge_type{
    int u,v,w;
    merge_type(int a=0, int b=0, int c=0):u(a), v(b), w(c){}
};

struct node_info{
    int id, degree;
    node_info(int nid = 0, int ndegree = 0):id(nid), degree(ndegree){}
    void operator =(const node_info &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
    }

    bool operator <(const node_info &tmp) const  // smaller one: larger degree, larger score, smaller id
    {
        if(degree==tmp.degree) return(id<tmp.id);
        else return(degree>tmp.degree);
    }
};

struct node_info2{
    int id, degree,score;
    node_info2(int nid = 0, int ndegree = 0, int nscore = 0):id(nid), degree(ndegree), score(nscore){}
    void operator =(const node_info2 &tmp)
    {
        id = tmp.id;
        degree = tmp.degree;
        score = tmp.score;
    }

    bool operator <(const node_info2 &tmp) const  // smaller one: smaller degree, larger score, smaller id
    {
        if(degree==tmp.degree)
        {
            if(score==tmp.score) return(id<tmp.id);
            else return(score>tmp.score);
        }
        else return(degree<tmp.degree);
    }
};


struct node_type{
    int degree, score;
    bool mis = false;
};

typedef vector<int> edge_type;

extern vector<node_type> g;
extern vector<edge_type> edge, backup_edge;

extern int n, kernel_n;
extern int m, kernel_m;
extern int misnum;

extern vector<int> alive;  // store the ids of alive vertices
extern vector<bool> del;   // identify which node is deleted during the peeling procedure
extern vector<bool> flag;

extern int num_d0, num_d1, num_d2; // the exact number of degree-one and degree-two vertices
extern vector<queue<int>> que; //que[i] store the nodes of degree i+1 (lazy update)
extern vector<int> chosen; // store the nodes chosen by inexact reduction
//extern set<node_info> bst;  // del_max_degree
extern set<node_info2> bst2;    // add_min_degree
extern int active_threshold, max_degree;
extern vector<merge_type> merge_log;

//graph operation
inline void add_edge(int u, int v)
{
    edge[u].push_back(v);
    edge[v].push_back(u);
    g[u].degree++;
    g[v].degree++;
}

inline bool check(int u, int & i)   // remove edge[u][i] if it has been deleted
{
    if(del[edge[u][i]]){
        edge[u][i] = edge[u].back();
        edge[u].pop_back();
        i--;
        return false;
    }
    else return true;
}

inline bool check(int & i)   // remove alive[i] if it has been deleted
{
    if(del[alive[i]]){
        alive[i] = alive.back();
        alive.pop_back();
        i--;
        return false;
    }
    else return true;
}

inline bool exist_edge(int u, int v)
{
    if(g[u].degree<g[v].degree)
    {
        for(int i=0; i<edge[u].size(); i++)
            if(check(u,i) && edge[u][i]==v) return true;
    }
    else
    {
        for(int i=0; i<edge[v].size(); i++)
            if(check(v,i) && edge[v][i]==u) return true;
    }
    return false;
}


void read_node_bin_data(string pathin);
void del_node(int u);
void backup_all_edges();
void resume_all_edges();
void check_mis();

//reduction function
alg_info degree_one_reduction();
alg_info degree_two_reduction();
void recheck_merge();
bool dom_check(int u);
alg_info brute_dom_reduction();
double dom_sample(int sample_time);

//greedy function
alg_info del_max_degree(bool batch);
alg_info add_min_degree(bool batch);

inline void update_score(int u)
{
    int & su = g[u].score;
    su = n;
    for(int i=0; i<edge[u].size(); i++)
        if(check(u,i)) su = min(su, g[edge[u][i]].degree);
}

bool bst2_LazyCheck(set<node_info2>::iterator iter);

//recover function
alg_info naive_recover();
